# Compilação e execução

Para compilar a aplicação, abra um shell na pasta test/app e execute o seguinte comando:

`mvn package`

Para executar a aplicação, execute o comando:

`mvn exec:java -Dstart-class="com.app.manager.SingleDigitApplication"`
    

# Execução dos testes unitários

Para executar os testes jUnit, execute o comando:

`mvn test`

# Comentários

- A operação de utilizar uma chave passada para o endpoint de decriptar as informações de um usuário não funciona. Não se sabe se porque a chave privada do usuário não está sendo retornada corretamente pelo endpoint responsável por essa funcionalidade ou se o problema é na conversão da String que representa a chave do usuário para um objeto do tipo PrivateKey.