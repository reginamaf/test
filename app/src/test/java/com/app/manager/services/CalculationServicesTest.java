package com.app.manager.services;

import static org.junit.Assert.*;

import org.junit.Test;

public class CalculationServicesTest {

	CalculationServices c = new CalculationServices();
	
	@Test
	public void testCalculateSingleDigitWith1Concatenation() {
		String n = "13";
		int k = 1;
		int expected = 4;
		int gotten = c.calculateSingleDigit(n, k);
		assertEquals(expected, gotten);
	}

	public void testCalculateSingleDigitWith5Concatenations() {
		String n = "1";
		int k = 5;
		int expected = 5;
		int gotten = c.calculateSingleDigit(n, k);
		assertEquals(expected, gotten);
	}
	
	public void testCalculateSingleDigitWithBigNumber() {
		String n = "9875";
		int k = 4;
		int expected = 8;
		int gotten = c.calculateSingleDigit(n, k);
		assertEquals(expected, gotten);
	}
}
