package com.app.manager.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Component
@Table(name = "resultados")
public class SingleDigitResult implements Serializable {

	private static final long serialVersionUID = -3385637546843901696L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id_result;
	
	@Column(name = "numero", nullable = false)
	private String n;
	
	@Column(name = "concatenacao", nullable = false)
	private int k;
	
	@Column(name = "resultado", nullable = false)
	private int result;
	
	@ManyToOne
    @JoinColumn(name="id", nullable = true)
    private User user;
	
	public SingleDigitResult() {
		
	}
	
	public SingleDigitResult(String n, int k, int result) {
		this.n = n;
		this.k = k;
		this.result = result;
	}
	
	public SingleDigitResult(String n, int k, int result, User user) {
		this.n = n;
		this.k = k;
		this.result = result;
		this.user = user;
	}

	@JsonIgnore
	public Long getId_result() {
		return id_result;
	}

	public void setId_result(Long id_result) {
		this.id_result = id_result;
	}

	public String getN() {
		return n;
	}

	public void setN(String n) {
		this.n = n;
	}

	public int getK() {
		return k;
	}

	public void setK(int k) {
		this.k = k;
	}

	public int getResult() {
		return result;
	}

	public void setResult(int result) {
		this.result = result;
	}

	@JsonIgnore
	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}
	
}
