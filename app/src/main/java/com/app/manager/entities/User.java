package com.app.manager.entities;
import java.io.Serializable;
import java.security.KeyPair;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Component
@Table(name = "usuario")
public class User implements Serializable {

	private static final long serialVersionUID = -6888542263201514002L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@Column(name = "nome", nullable = false)
	private String name;

	@Column(name = "email", nullable = false)
	private String email;
	
	@OneToMany(mappedBy="user")
	private List<SingleDigitResult> results;
	
	@Column(name = "chave", length = 2048, nullable = false)
	private KeyPair keys;

	public User() {
		
	}
	
	public User(Long id, String name, String email, KeyPair keys) {
		this.name = name;
		this.email = email;
		this.keys = keys;
		this.results = new ArrayList<SingleDigitResult>();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	
	@JsonIgnore
	public KeyPair getKeys() {
		return keys;
	}

	public void setKeys(KeyPair keys) {
		this.keys = keys;
	}

	public List<SingleDigitResult> getResults() {
		return this.results;
	}
	
	public void addResult (SingleDigitResult result) {
		this.results.add(result);
	}
	
	public void setResults(List<SingleDigitResult> results) {
		this.results = results;
	}
	
	@Override
	public String toString() {
		return "UserDto [id=" + id + ", Nome=" + name + ", email=" + email + "]";
	}

}