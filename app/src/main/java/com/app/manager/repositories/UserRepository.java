package com.app.manager.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.app.manager.entities.User;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {
}