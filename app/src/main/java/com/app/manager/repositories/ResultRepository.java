package com.app.manager.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.app.manager.entities.SingleDigitResult;

@Repository
public interface ResultRepository extends JpaRepository<SingleDigitResult, Long> {
}
