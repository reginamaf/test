package com.app.manager;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Configuration;

@SpringBootApplication
@Configuration
public class SingleDigitApplication {

	public static void main(String[] args) {
		SpringApplication.run(SingleDigitApplication.class, args);
	}
}