package com.app.manager.controller;

import java.net.URI;
import java.security.InvalidKeyException;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.util.List;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.app.manager.dtos.UserDto;
import com.app.manager.entities.SingleDigitResult;
import com.app.manager.entities.User;
import com.app.manager.responses.Response;
import com.app.manager.services.UserServices;

import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/api/users")
public class SingleDigitController {

	@Autowired
	private UserServices userService;
	
	SingleDigitController() throws NoSuchAlgorithmException {
		KeyPairGenerator.getInstance("RSA").generateKeyPair();
	}

	// Create User
	@ApiOperation(
			value="Cadastrar um novo usuário", 
			response=Response.class, 
			notes="Este endpoint salva um novo usuário.")
	@PostMapping(path = "/new")
	public ResponseEntity<Response<User>> register(@Valid @RequestBody UserDto userDto, BindingResult result) throws InvalidKeyException, IllegalBlockSizeException, BadPaddingException, NoSuchAlgorithmException, NoSuchPaddingException {
		Response<User> response = new Response<User>();

		if (result.hasErrors()) {
			result.getAllErrors().forEach(error -> response.getErrors().add(error.getDefaultMessage()));
			return ResponseEntity.badRequest().body(response);
		}

		User registeredUser = this.userService.save(userDto);
		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(userDto.getId())
				.toUri();
		response.setData(registeredUser);
		return ResponseEntity.created(location).body(response);
	}

	// Read User
	@ApiOperation(
			value="Listar usuários", 
			response=Response.class, 
			notes="Este endpoint lista todos os usuários cadastrados.")
	@GetMapping
	public ResponseEntity<List<User>> list() {
		List<User> users = userService.list();
		return ResponseEntity.status(HttpStatus.OK).body(users);
	}

	@ApiOperation(
			value="Encontrar usuário", 
			response=Response.class, 
			notes="Este endpoint lista os dados de um usuário específico.")
	@GetMapping(path = "/{id}")
	public ResponseEntity<Response<User>> find(@PathVariable("id") Long id) throws Exception {
		Response<User> response = new Response<User>();
		User user = null;
		try {
			user = userService.find(id);
		} catch (Exception e) {
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body(response);
		}
		response.setData(user);
		return ResponseEntity.status(HttpStatus.OK).body(response);
	}
	
	@ApiOperation(
			value="Listar chave de um usuário específico (para testes)", 
			response=Response.class, 
			notes="Este endpoint retorna a chave de um usuário.")
	@GetMapping(path = "/key/{id}")
	public ResponseEntity<Response<byte[]>> getUserKey(@PathVariable("id") Long id) throws Exception {
		Response<byte[]> response = new Response<byte[]>();
		User user = null;
		try {
			user = userService.find(id);
		} catch (Exception e) {
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body(response);
		}
		response.setData(user.getKeys().getPrivate().getEncoded());
		return ResponseEntity.status(HttpStatus.OK).body(response);
	}
	
	@ApiOperation(
			value="Encontrar usuário com chave", 
			response=Response.class, 
			notes="Este endpoint lista os dados decriptados de um usuário específico.")
	@GetMapping(path = "/findWithKey/{id}")
	public ResponseEntity<Response<User>> findWithKey(@PathVariable("id") Long id,
													  @RequestParam("chave") String keyStr) throws Exception {
		Response<User> response = new Response<User>();
		User user = null;
		try {
			user = userService.find(id, keyStr);
		} catch (Exception e) {
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body(response);
		}
		response.setData(user);
		return ResponseEntity.status(HttpStatus.OK).body(response);
	}
	
	// Update User
	@ApiOperation(
			value="Atualizar usuário", 
			response=Response.class, 
			notes="Este endpoint atualiza um usuário específico.")
	@PostMapping(path = "/update/{id}")
	public ResponseEntity<Response<User>> update(@PathVariable("id") Long id, @Valid @RequestBody UserDto userDto, BindingResult result) throws Exception {
		Response<User> response = new Response<User>();

		if (result.hasErrors()) {
			result.getAllErrors().forEach(error -> response.getErrors().add(error.getDefaultMessage()));
			return ResponseEntity.badRequest().body(response);
		}
		User registeredUser = null;
		try {
			registeredUser = this.userService.update(id, userDto);
		} catch (Exception e) {
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body(response);
		}
		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(userDto.getId())
				.toUri();
		response.setData(registeredUser);
		return ResponseEntity.created(location).body(response);
	}
	
	// Delete User
	@ApiOperation(
			value="Deletar usuário", 
			response=Response.class, 
			notes="Este endpoint deleta um usuário específico.")
	@RequestMapping(value ="/delete/{id}", method = RequestMethod.DELETE)
	public HttpStatus delete(@PathVariable("id") Long id) throws Exception {
		try {
			userService.delete(id);
		} catch (Exception e) {
			return HttpStatus.NOT_FOUND;
		}
		 return HttpStatus.OK;
	}
	
	// Calculate Single Digit
	@ApiOperation(
			value="Calcular digito único para um usuário", 
			response=Response.class, 
			notes="Este endpoint calcula o dígito único para um número específico e salva esse valor na lista de"
					+ "resultados de um usuário.")
	@PostMapping(path = "/calculateForUser/{id}")
	public ResponseEntity<Response<SingleDigitResult>> calculateForUser(@PathVariable("id") Long id,
																		@RequestParam("numero") String n, 
																 	    @RequestParam("concatenacao") int k) throws Exception {
		Response<SingleDigitResult> response = new Response<SingleDigitResult>();
		try {
			SingleDigitResult result = userService.calculateForUser(id, n, k);
			response.setData(result);
		} catch (Exception e) {
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body(response);
		}

		return ResponseEntity.status(HttpStatus.OK).body(response);
	}
	
	@ApiOperation(
			value="Calcular digito único", 
			response=Response.class, 
			notes="Este endpoint calcula o dígito único para um número específico.")
	@GetMapping(path = "/calculate")
	public ResponseEntity<Response<SingleDigitResult>> calculate(@RequestParam("numero") String n, 
			 												     @RequestParam("concatenacao") int k) {
		SingleDigitResult result = userService.calculate(n, k);
		Response<SingleDigitResult> response = new Response<SingleDigitResult>();
		response.setData(result);
		return ResponseEntity.status(HttpStatus.OK).body(response);
	}
	
	// Get All Results for User
	@ApiOperation(
			value="Listar resultados para usuário", 
			response=Response.class, 
			notes="Este endpoint lista todos os resultados de dígito único para um usuário específico.")
	@GetMapping(path = "results/{id}")
	public ResponseEntity<Response<List<SingleDigitResult>>> results(@PathVariable("id") Long id) throws Exception {
		Response<List<SingleDigitResult>> response = new Response<List<SingleDigitResult>>();
		User user = null;
		try {
			user = userService.find(id);
		} catch (Exception e) {
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body(response);
		}
		response.setData(user.getResults());
		return ResponseEntity.status(HttpStatus.OK).body(response);
	}
	 
}