package com.app.manager.services;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;

@Service
public class CalculationServices {
	private Map<String,Integer> m_cacheSingleDigit;
	private List<String> m_cacheControl;
	
	public CalculationServices() {
		this.m_cacheSingleDigit = new HashMap<String,Integer>();
		this.m_cacheControl = new ArrayList<String>();
	}
	
	private void addToCache(String n, int result) {
		if (m_cacheControl.size() < 10) {
			m_cacheControl.add(n);
		} else {
			m_cacheSingleDigit.remove(m_cacheControl.get(0));
			m_cacheControl.remove(0);
			m_cacheControl.add(n);
		}
		m_cacheSingleDigit.put(n, result);
	}
	
	private int sumDigits(String number) {
		int sum = 0;
		for (int i = 0; i < number.length(); ++i){
		    char c = number.charAt(i);        
		    sum += Character.getNumericValue(c);
		}
		return sum;
	}
	
	public int calculateSingleDigit(String n, int k) {
		if (m_cacheSingleDigit.containsKey(n)) {
			return m_cacheSingleDigit.get(n);
		} else {
			StringBuilder builder = new StringBuilder(k * n.length());
			   for (int i = 0; i < k; ++i) {
			   builder.append(n);
			}
			String number = builder.toString();
			
			int result = sumDigits(number);
			while (result >= 10) {
				number = Integer.toString(result);
				result = sumDigits(number);
			}
			addToCache(n, result);
			return result;
		}
	}
}
