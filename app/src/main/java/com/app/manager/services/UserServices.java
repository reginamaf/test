package com.app.manager.services;

import java.security.InvalidKeyException;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.util.List;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.app.manager.dtos.UserDto;
import com.app.manager.entities.SingleDigitResult;
import com.app.manager.entities.User;
import com.app.manager.repositories.ResultRepository;
import com.app.manager.repositories.UserRepository;

@Service
public class UserServices {

	@Autowired
	private UserRepository userRepository;
	@Autowired
	private ResultRepository resultRepository;
	@Autowired
	private CalculationServices calculator;

	private String encrypt(String target, PublicKey key) throws InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException {
		Cipher cipher = Cipher.getInstance("RSA");
		cipher.init(Cipher.ENCRYPT_MODE, key);
		byte[] encrypted = cipher.doFinal(target.getBytes());
		return encrypted.toString();
	}
	
	private String decrypt(String target, PrivateKey key) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException {
		Cipher cipher = Cipher.getInstance("RSA");
		cipher.init(Cipher.DECRYPT_MODE, key);
		byte[] encrypted = cipher.doFinal(target.getBytes());
		return encrypted.toString();
	}
	
	// Create
	public User save(UserDto userDto) throws IllegalBlockSizeException, BadPaddingException, InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException {
		User user = new User();

		KeyPair keys = KeyPairGenerator.getInstance("RSA").generateKeyPair();
		user.setName(encrypt(userDto.getName(), keys.getPublic()));
		user.setEmail(encrypt(userDto.getEmail(), keys.getPublic()));
		user.setResults(userDto.getResults());
		user.setKeys(keys);
		
		return userRepository.save(user);
	}

	// Read
	public User find(Long id) throws Exception {
		User user = userRepository.findOne(id);

		if (user == null) {
			throw new Exception("Usuário não cadastrado");
		}
		return user;
	}
	
	public User find(Long id, String keyStr) throws Exception {
		User user = userRepository.findOne(id);

		if (user == null) {
			throw new Exception("Usuário não cadastrado");
		}
		PrivateKey key = user.getKeys().getPrivate();
		if (/*Check if keys are equal*/true) {
			user.setName(decrypt(user.getName(), key));
			user.setEmail(decrypt(user.getEmail(), key));
		} else {
			throw new Exception("Chave inválida");
		}
		return user;
	}
	
	public List<User> list() {
		return userRepository.findAll();
	}
	
	// Update
	public User update(Long id, UserDto updated) throws Exception {
		User user = find(id);
		
		KeyPair keys = user.getKeys();
		user.setName(encrypt(updated.getName(), keys.getPublic()));
		user.setEmail(encrypt(updated.getEmail(), keys.getPublic()));
		user.setKeys(keys);
		user.setId(id);
		
		return userRepository.saveAndFlush(user);
	}
	
	public SingleDigitResult calculateForUser(Long id, String n, int k) throws Exception {
		User user = find(id);
		int result = calculator.calculateSingleDigit(n, k);
		SingleDigitResult sdResult = new SingleDigitResult(n, k, result, user);
		user.addResult(resultRepository.saveAndFlush(sdResult));
		userRepository.saveAndFlush(user);
		return sdResult;
	}
	
	// Delete
	public void delete(Long id) throws Exception {
		find(id);
		userRepository.delete(id);
	}
	
	public SingleDigitResult calculate(String n, int k) {
		int result = calculator.calculateSingleDigit(n, k);
		return new SingleDigitResult(n, k, result);
	}
}