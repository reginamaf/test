package com.app.manager.dtos;

import com.app.manager.entities.SingleDigitResult;
import java.io.Serializable;
import java.util.List;

import javax.validation.constraints.NotNull;

public class UserDto implements Serializable {

	private static final long serialVersionUID = -8105241933692707649L;

	private Long id;
	private String name;
	private String email;
	private List<SingleDigitResult> results;

	public UserDto() {

	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@NotNull(message = "Nome é uma informação obrigatória")
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@NotNull(message = "Email é uma informação obrigatória")
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	
	public List<SingleDigitResult> getResults() {
		return results;
	}
	
	public void addResult (SingleDigitResult result) {
		results.add(result);
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder(results.size());
		   for (int i = 0; i < results.size(); ++i) {
		   builder.append(results.get(i).toString());
		}
		String resultStr = builder.toString();
		
		return "UserDto [id=" + id + ", Nome=" + name + ", email=" + email + 
				", Resultados=" + resultStr + "]";
	}
}